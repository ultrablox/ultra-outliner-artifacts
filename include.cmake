function(try_add_include dir)
  if(EXISTS "${dir}")
    include_directories(${dir})
    message(STATUS "INCLUDE_PATHS+=${dir}")
  endif()
endfunction(try_add_include)

function(try_add_libs dir)
  if(EXISTS "${dir}")
    link_directories(${dir})
    message(STATUS "LIBS_PATHS+=${dir}")
  endif()
endfunction(try_add_libs)

function(try_add_cmake dir)
  if(EXISTS "${dir}")
    list(APPEND CMAKE_MODULE_PATH "${dir}")
    set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" CACHE INTERNAL "MODULE_PATH")
    message(STATUS "CMAKE_MODULES+=${dir}")
  endif()
endfunction(try_add_cmake)

function(add_artifact dir)
  message(STATUS "Including dependency: ${dir}")
  list(APPEND CMAKE_PREFIX_PATH "${dir}")
  set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH}" CACHE INTERNAL "PREFIX_PATH")

  try_add_cmake(${dir}/cmake)
  try_add_cmake(${dir}/lib/cmake)

  try_add_include(${dir}/include)
  try_add_include(${dir}/include/ZXing) # hack
  try_add_include(${dir}/include/opencv4) # hack

  try_add_libs(${dir}/lib)
  try_add_libs(${dir}/lib64)
  try_add_libs(${dir}/plugins/platforms)
  try_add_libs(${dir}/plugins/printsupport)
  try_add_libs(${dir}/plugins/platformthemes)
  try_add_libs(${dir}/plugins/imageformats)
  try_add_libs(${dir}/plugins/sqldrivers)
  try_add_libs(${dir}/plugins/audio)
  try_add_libs(${dir}/plugins/mediaservice)

  # For MSVC
  list(APPEND CMAKE_BIN_PATH "${dir}/bin")
  set(CMAKE_BIN_PATH "${CMAKE_BIN_PATH}" CACHE INTERNAL "BIN_PATH")
endfunction(add_artifact)


function(include_artifact BASE_DIR BUILD_PLATFORM)
  set(ARTIFACTS_DIR "${BASE_DIR}/3rdparty/${BUILD_PLATFORM}" CACHE INTERNAL "ARTIFACTS_PLATFORM_PATH")
  try_add_cmake(${ARTIFACTS_DIR})
  include(PlatformSpecs)
  init_compiler()

  set(CMAKE_BIN_PATH "PATH=%PATH%" CACHE INTERNAL "BIN_PATH")

  file(GLOB CHILDREN true "${ARTIFACTS_DIR}/*")
  FOREACH(subdir ${CHILDREN})
    get_filename_component(absArtifactPath ${subdir} ABSOLUTE)
    add_artifact(${absArtifactPath})
  ENDFOREACH()
  
  message(WARNING "Use for MSVC Debuggin environment: ${CMAKE_BIN_PATH}")
endfunction(include_artifact)
